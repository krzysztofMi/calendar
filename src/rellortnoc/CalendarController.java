package rellortnoc;

import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;


import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class CalendarController {

    @FXML
    GridPane calendarGrid;

    @FXML
    Label monthYear;


    Calendar calendar;
    ArrayList<Label> dates;

    public void initialize(){
        calendar = Calendar.getInstance();
        dates = new ArrayList<>();
        monthYear.setText(getMonthFromInt(calendar.get(Calendar.MONTH), Locale.UK)+" "+calendar.get(Calendar.YEAR));
        setDays();
        addDaysToGrid();
    }

    private String getMonthFromInt(int month, Locale locale ) {
        DateFormatSymbols dfs = new DateFormatSymbols(locale);
        return dfs.getMonths()[month];

    }

    private Integer getfirstDayOfMonth(Integer month){
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 0);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    private void setDays(){
        dates.clear();
        int max = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for(int i = 0; i<max; i++){
            dates.add(new Label(Integer.toString(i+1)));
        }
    }

    private void addDaysToGrid(){
        int first = getfirstDayOfMonth(calendar.get(Calendar.MONTH));
        int row = 2;
        for(Label date: dates){
            if(first>7){
                first = 1;
                row++;
            }
            calendarGrid.add(date, first++, row);
            GridPane.setHalignment(date, HPos.CENTER);
        }

    }


    private void clearCalendarGrid(){
        dates.stream().forEach(e->calendarGrid.getChildren().remove(e));
    }

    @FXML
    public void moveLeft(){
        clearCalendarGrid();
        int month = calendar.get(Calendar.MONTH);
        if(month<0){
            month = 11;
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)-1);
        }
        calendar.set(Calendar.MONTH, month-1);
        monthYear.setText(getMonthFromInt(calendar.get(Calendar.MONTH), Locale.UK)+" "+calendar.get(Calendar.YEAR));
        setDays();
        addDaysToGrid();
    }

    @FXML
    public void moveRight(){
        clearCalendarGrid();
        int month = calendar.get(Calendar.MONTH);
        if(month>10){
            month = -1;
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)+1);
        }
        calendar.set(Calendar.MONTH, month+1);
        monthYear.setText(getMonthFromInt(calendar.get(Calendar.MONTH), Locale.UK)+" "+calendar.get(Calendar.YEAR));
        setDays();
        addDaysToGrid();
    }

}
