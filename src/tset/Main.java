package tset;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
  
public class Main  extends Application {

        @Override
        public void start(Stage stage) throws Exception {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/res/Calendar.fxml"));
            Pane root = new Pane();
            root = loader.load();
            Scene scene = new Scene(root);

            scene.getStylesheets().add(getClass().getResource("/res/apple").toString());
            stage.setScene(scene);
            stage.setTitle("Calendar");
            stage.setResizable(false);
            stage.show();
        }


        public static void main(String[] args) {
            launch(args);
        }
}


